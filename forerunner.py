# Python Script to Save And Convert Garmin Forerunner 305 files
# !/user/bin/python3

import subprocess
import webbrowser
import getpass
import re
import os
from datetime import datetime

user = getpass.getuser()
now = datetime.now()
print("\n" + now.strftime("%Y-%m-%d %H:%M:%S"), "\n" + "Welcome", user + "! \n")

""" Check If The Device Is Connected """
device_check = subprocess.Popen(['/usr/bin/garmin_get_info'], shell=False, stdout=subprocess.PIPE)
stdout, stderr = device_check.communicate()

device_info = stdout.decode('utf-8').split('\n')
n = len(list(device_info))
product = list(device_info[1])

""" Feedback of Connection State """
if n > 2:
    print("Status: Connected with", device_info[0][1:26], "\n")

    """ User Storage Location """
    while 1:
        directory = input("Please enter a storage location: \n").strip()
        # Add slash to location if not specified
        if directory.endswith("/"):
            pass
        else:
            directory = directory + "/"
        try:
            os.chdir(os.path.abspath(directory))
            print("")
            break
        except FileNotFoundError:
            print("Error: The Storage location does not exist. \n")

    """ Save GMN Files From Device """
    save_files = subprocess.Popen(['/usr/bin/garmin_save_runs'], cwd=directory, shell=False, stdout=subprocess.PIPE)
    stdout, stderr = save_files.communicate()

    all_files = stdout.decode('utf-8')
    print(all_files)

    skipped_files = re.findall("Skipped:", all_files)
    saved_files = re.findall("Wrote:", all_files)

    sk = len(list(skipped_files))
    sv = len(list(saved_files))

    """ Convert GMN To GPX File(s) """
    # Paths of new added files
    sf_paths = re.findall("Wrote:(.*)", all_files)
    sf = len(sf_paths)

    if sv > 0:
        for gmn_file in sf_paths:
            convert_files = subprocess.Popen(['/usr/bin/garmin_gpx', gmn_file.strip()], shell=False,
                                             stdout=subprocess.PIPE)
            stdout, stderr = convert_files.communicate()

            try:
                f = open("{}.gpx".format(gmn_file.split(".")[0].strip()), "w+")
                f.write(stdout.decode('utf8'))
                f.close()
            except Exception as e:
                print(f"Error: {e} \n")

        """ Split From The Right Of The String And Remove Duplicates From List """
        # (1) new_directories = [(gmn_file.strip().split('/')[1:8]) for gmn_file in sf_paths]
        # (2) test = list(dict.fromkeys(new_directories))
        new_directories = list(dict.fromkeys([(gmn_file.strip().rsplit('/', 1)[0]) for gmn_file in sf_paths]))
        sd = len(new_directories)

        """ Distinctions Of Singular And Plural In Printed Output """
        def numerus(sk, sv, sf, sd):
            t1 = "S{}ed File{}:"
            if sk <= 1:
                print(t1.format("kipp", "") + "    " + str(sk))
            if 2 <= sk <= 9:
                print(t1.format("kipp", "s") + "   " + str(sk))
            if 10 <= sk <= 99:
                print(t1.format("kipp", "s") + "  " + str(sk))
            if sv == 1:
                print(t1.format("av", "") + "      " + str(sv))
            if 2 <= sv <= 9:
                print(t1.format("av", "s") + "     " + str(sv))
            if 10 <= sv <= 99:
                print(t1.format("av", "s") + "    " + str(sv))
            print("")
            t2 = "{} written to the following {}: \n"
            if sv < 2:
                print(t2.format("This file was", "directory"))
            if sv >= 2:
                if sd < 2:
                    print(t2.format("These files were", "directory"))
                if sd >= 2:
                    print(t2.format("These files were", "directories"))
            # Printing each list element in new line
            print(*new_directories, sep="\n")
            print("")
            t3 = "The following {}gmn {} in {} will be converted to {}gpx {}:"
            if sf < 2:
                print(t3.format("", "file", "this directory", "a ", "file"))
            if sf >= 2:
                if sd < 2:
                    print(t3.format(str(sf) + " ", "files", "this directory", "", "files"))
                if sd >= 2:
                    print(t3.format(str(sf) + " ", "files", "these directories", "", "files"))
            print("")
            # Loop to show saved gmn and converted gpx files
            for gmn_file in sf_paths:
                print(gmn_file.rsplit('/', 1)[1].strip(), "  -->  ",
                      "{}.gpx".format(gmn_file.rsplit('/', 1)[1].strip().split(".")[0]))
            print("")
            if sf < 2:
                print("Showing this converted file with gpxviewer ...")
            if sf >= 2:
                print("Showing these", sf, "converted files with gpxviewer ...")
            print("")


        numerus(sk, sv, sf, sd)

        """ Open List Of New Generated GPX File(s) """
        new_files = [("{}.gpx".format(gmn_file.split(".")[0]).strip()) for gmn_file in sf_paths]

        for file in new_files:
            try:
                show_files = subprocess.Popen(['/usr/bin/gpxviewer', file], shell=False, stdout=subprocess.PIPE)
                stdout, stderr = show_files.communicate()
            except FileNotFoundError:
                print("Error: The script uses gpxviewer, please install the package on your system.\n")
                break
    else:
        print("No file was written. \n")

else:
    print("Status: Not connected,", list(device_info)[0], "\n")


""" Open Upload To Runalyze """
def runalyze(feedback):
    while feedback not in ("y", "n"):
        feedback = str(input("Do you want to upload some files to runalyze.com? [y/n] ").lower().strip())
        if feedback[:1] == 'y':
            print("The runalyze upload URL is opening. \n" + "Bye!")
            webbrowser.open_new_tab("https://runalyze.com/activity/new-upload")
        elif feedback[:1] == 'n':
            print("You've decided not to open the runalyze upload option. \n" + "Bye!")
        else:
            print('Please enter "y" or "n".')
        print("")


feedback = None
runalyze(feedback)
